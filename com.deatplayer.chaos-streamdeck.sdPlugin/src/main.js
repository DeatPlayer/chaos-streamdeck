let websocket = null;
let pluginUUID = null;
let settingsCache = {};

const controlAction = {

    type: "com.deatplayer.chaos-streamdeck.control",

    onKeyUp: function(context, settings) {
        var clockIp = "",
        var auth = "";
        var control = "";
        var controlValue = "";

        if(settings['clockIp'] != null) {
            clockIp = settings['clockIp'];
        }

        if(settings['auth'] != null) {
            auth = settings['auth'];
        }

        if(settings['control'] != null) {
            control = settings['control'];
        }

        if(settings['controlValue'] != null) {
            controlValue = settings['controlValue'];
        }

        if(clockIp && auth && control) {
            setChaosClockControl(clockIp, auth, control, controlValue, context);
        }
    },

    onWillAppear: function(context, settings) {
        settingsCache[context] = settings;
    },

    SetSettings: function(context, settings) {
        const json = {
            "event": "setSettings",
            "context": context,
            "payload": settings
        };

        websocket.send(JSON.stringify(json));
    },

    SendSettings : function(action, context) {
        const json = {
            "action": action,
            "event": "sendToPropertyInspector",
            "context": context,
            "payload": settingsCache[context]
        };

        websocket.send(JSON.stringify(json));
    },

    SetTitle : function(context,jsonPayload) {
        //console.log(jsonPayload['title']);
        let payload = {};
        payload.title = jsonPayload['title'];
        payload.target = "DestinationEnum.HARDWARE_AND_SOFTWARE";
        const json = {
            "event": "setTitle",
            "context": context,
            "payload": payload,
        };
        websocket.send(JSON.stringify(json));
    }
};




function setChaosClockControl(ip,auth,control,controlValue,context)
{
    var clockURL = "http://"+ip+":80/api";
    var controlData = null;

    if(ip != "undefined" && auth != "undefined" && control){
        
        switch (control){
            case "DrawStaticImage" :
                controlData = {
                    "topic": "matrix/drawStaticimage",
                    "payload": controlValue
                };
            break;
            case "ScrollText" :
                controlData = {
                    "topic": "matrix/scrollText",
                    "payload": controlValue
                };
            break;
            case "ChangeMode" :
                controlData = {
                    "topic": "matrix/changeMode",
                    "payload": "'" + controlValue + "'"
                };
            break;
        }
        console.log(controlData);
        var request = new XMLHttpRequest();
        
        request.open("POST", clockURL, true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(JSON.stringify(controlData));

        request.onreadystatechange = function() {
            if (request.readyState == 4){
                if (request.status == 204) {
                    //var status = request.status;
                    //var data = JSON.parse(request.responseText);
                    //console.log("effect changed");
                    showAlert("showOk",context);                }
                else {
                    //console.log("Shit went wrong");
                    showAlert("showAlert",context);
                }
            }
        }
    
    }
}

function showAlert(event,context) {
    if (websocket) {
        let payload = {};
        const json = {
            "event": event,
            "context": context,
        };
        websocket.send(JSON.stringify(json));
    }
}

function connectSocket(inPort, inPluginUUID, inRegisterEvent, inInfo)
{
    pluginUUID = inPluginUUID;

    // Open the web socket
    websocket = new WebSocket("ws://localhost:" + inPort);

    function registerPlugin(inPluginUUID)
    {
        const json = {
            "event": inRegisterEvent,
            "uuid": inPluginUUID
        };

        websocket.send(JSON.stringify(json));
    };

    websocket.onopen = function()
    {
        // WebSocket is connected, send message
        registerPlugin(pluginUUID);
    };

    websocket.onmessage = function (evt)
    {
        // Received message from Stream Deck
        const jsonObj = JSON.parse(evt.data);
        const event = jsonObj['event'];
        const action = jsonObj['action'];
        const context = jsonObj['context'];
        const jsonPayload = jsonObj['payload'];

        if(event == "keyUp")
        {
            const settings = jsonPayload['settings'];
            switch (action) {
                case "com.deatplayer.chaos-streamdeck.control" :
                    controlAction.onKeyUp(context, settings);
                break;
            }
        }
        else if(event == "willAppear")
        {
            const settings = jsonPayload['settings'];
            switch (action) {
                case "com.deatplayer.chaos-streamdeck.control" :
                    controlAction.onWillAppear(context, settings);
                break;
            }
        }
        else if(event == "sendToPlugin") {

            if(jsonPayload['type'] == "updateSettings") {
                switch (action) {
                    case "com.deatplayer.chaos-streamdeck.control" :
                        controlAction.SetSettings(context, jsonPayload);
                    break;
                }
                settingsCache[context] = jsonPayload;

            } else if(jsonPayload['type'] == "requestSettings") {
                switch (action) {
                    case "com.deatplayer.chaos-streamdeck.control" :
                        controlAction.SendSettings(action, context);
                    break;
                }
            } else if (jsonPayload['type'] == "setTitle") {
                switch (action) {
                    case "com.deatplayer.chaos-streamdeck.control" :
                        controlAction.SetTitle(context, jsonPayload);
                    break;
                }
            }
        }
    };
};