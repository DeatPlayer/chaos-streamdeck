let websocket = null,
    uuid = null,
    actionInfo = {};

function connectSocket(inPort, inUUID, inRegisterEvent, inInfo, inActionInfo) {
    uuid = inUUID;

    actionInfo = JSON.parse(inActionInfo);
    //console.log(actionInfo);
    websocket = new WebSocket('ws://localhost:' + inPort);

    websocket.onopen = function () {
        const json = {
            event: inRegisterEvent,
            uuid: inUUID
        };
        websocket.send(JSON.stringify(json));
        requestSettings();
    };

    websocket.onmessage = function (evt) {
        // Received message from Stream Deck
        const jsonObj = JSON.parse(evt.data);
        const action = jsonObj.action;
        if (jsonObj.event === 'sendToPropertyInspector') {
            const payload = jsonObj.payload;
            if (payload.error) {
                return;
            }
            const ip = document.getElementById('ip');
            ip.value = payload.ip;

            const auth = document.getElementById('auth');
            auth.value = payload.auth;

            const pair = document.getElementById('pair');

            if (ip.value == "undefined") {
                ip.value = "";
            }

            if (auth.value == "undefined" || auth.value == "Start Nanoleaf Pairing Then Hit --->") {
                auth.value = "";
            }

            if (ip.value != "" && auth.value == "") {
                pair.hidden = false;
            }
            else {
                pair.hidden = true;
            }
            switch (action) {
                case "com.deatplayer.chaos-streamdeck.control":
                    controlPanel(action);
                    const control = document.getElementById('control');
                    const controlvalue = document.getElementById('controlvalue');
                    const valuelabel = document.getElementById('valuelabel');
                    savedControl = payload.control;
                    savedValue = payload.controlvalue;
                    if (savedValue) {
                        controlvalue.value = savedValue;
                    }
                    if (!savedControl) {
                        savedControl = "On";
                    }
                    var controls = ["On", "Off", "Brightness -", "Brightness +", "Brightness"];
                    for (index in controls) {
                        control.options[control.options.length] = new Option(controls[index], controls[index]);
                        if (savedControl == controls[index]) {
                            control.options[index].selected = true;
                        }
                    }
                    if (savedControl == "On" || savedControl == "Off") {
                        controlvalue.style = "visibility: hidden";
                        valuelabel.hidden = true;
                        console.log("hide");
                    }
                    else {
                        controlvalue.style = "visibility: visible";
                        valuelabel.hidden = false;
                        console.log("show");
                    }

                    break;

            }

            const el = document.querySelector('.sdpi-wrapper');
            el && el.classList.remove('hidden');
        }
    };

}

function valueLimit(element) {
    var max_chars = 3;
    if (element.value.length > max_chars) {
        element.value = element.value.substr(0, max_chars);
    }

    if (element.value > 100) {
        element.value = 100;
    }
}

function controlPanel(action) {
    console.log(action);
    panel = document.getElementById('panel');
    panel.innerHTML = '        <div class="sdpi-item"> \
    <div class="sdpi-item-label">Control mode</div> \
    <select class="sdpi-item-value select" id="control" onchange="updateSettings(\''+ action + '\')"> \
    </select> \
    <div class="sdpi-item-label" id="valuelabel">Payload</div> \
    <input value="0" type="text" id="controlvalue" onChange="updateSettings(\''+ action + '\')"> \
    </div>';
}

function requestSettings() {
    if (websocket) {
        let payload = {};
        payload.type = "requestSettings";
        const json = {
            "action": actionInfo['action'],
            "event": "sendToPlugin",
            "context": uuid,
            "payload": payload,
        };
        websocket.send(JSON.stringify(json));
    }
}

function setTitle(newTitle) {
    if (websocket) {
        let payload = {};
        payload.type = "setTitle";
        payload.title = newTitle;
        payload.target = "DestinationEnum.HARDWARE_AND_SOFTWARE";
        const json = {
            "action": actionInfo['action'],
            "event": "sendToPlugin",
            "context": uuid,
            "payload": payload,
        };
        //console.log(json);
        websocket.send(JSON.stringify(json));
    }
}

function updateSettings(action) {
    if (websocket) {
        let payload = {};

        payload.type = "updateSettings";

        const ip = document.getElementById('ip');
        payload.ip = ip.value;

        const auth = document.getElementById('auth');
        if (auth.value == "") {
            const pair = document.getElementById('pair');
            pair.hidden = false;
        }
        else {
            pair.hidden = true;
            payload.auth = auth.value;
        }
        switch (action) {
            case "com.deatplayer.chaos-streamdeck.control":
                const control = document.getElementById('control');
                const controlvalue = document.getElementById('controlvalue');
                const valuelabel = document.getElementById('valuelabel');
                payload.control = control.options[control.selectedIndex].value;
                payload.controlvalue = controlvalue.value;
                //console.log(payload);
                if (payload.control == "On" || payload.control == "Off") {
                    controlvalue.style = "visibility: hidden";
                    valuelabel.hidden = true;
                    //console.log("hide");
                }
                else {
                    controlvalue.style = "visibility: visible";
                    valuelabel.hidden = false;
                    //console.log("show");
                }
                break;
            case "com.deatplayer.chaos-streamdeck.effects":
                const effects = document.getElementById('effects');
                if (effects.length > 0) {
                    payload.effect = effects.options[effects.selectedIndex].value;
                    payload.effects = {};
                    for (index in effects.options) {
                        payload.effects[index] = effects.options[index].value;
                    }
                    break;
                }

        }

        //console.log(payload);
        const json = {
            "action": actionInfo['action'],
            "event": "sendToPlugin",
            "context": uuid,
            "payload": payload,
        };
        websocket.send(JSON.stringify(json));
    }
}
