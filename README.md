# chaos-streamdeck
Stream deck plugin for the chaos crew.
---
The plugin is primarily intended to control the API of the [chaos-clock](https://gitlab.com/wirelos/chaos-clock) via a stream deck and to execute various functions there. The further development (which still everything at functionality comes in addition) is still open.


**Note:** The project is still completely untested and in the middle of development. As soon as the first tests have been done and the basic functionality is given, there will also be a short tutorial on how to use the plugin here.
---

For the development of this plugin I have oriented myself very strongly to [this](https://github.com/Baptiewright/streamdeck-nanoleaf) project, since I have so far hardly any experience with JavaScript or the Stream Deck.